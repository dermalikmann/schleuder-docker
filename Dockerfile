FROM ruby:3.0
LABEL maintainer Softwerke.md <admin [@] softwerke.md>

ENV DEBIAN_FRONTEND noninteractive

ENV SCHLEUDER_VERSION="schleuder-4.0.0"
ENV SCHLEUDER_CLI_VERSION="schleuder-cli-0.1.0"



RUN groupadd -g 201 schleuder && useradd -g schleuder -u 201 -d /var/lib/schleuder schleuder && \
    apt-get update && apt-get install -y \
    libmagic-dev \
    libgpgme11-dev \
    gnupg2 \
    wget \
    git \
    git-core \
    inotify-tools \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*

# get and install schleuder
RUN git clone -b ${SCHLEUDER_VERSION} --single-branch --depth 1 https://0xacab.org/schleuder/schleuder.git /opt/schleuder && \
    cd /opt/schleuder && \
    bundle install

RUN git clone -b ${SCHLEUDER_CLI_VERSION} --single-branch --depth 1 https://0xacab.org/schleuder/schleuder-cli.git /opt/schleuder-cli && \
    cd /opt/schleuder-cli && \
    # remove accidental Gemfile.lock
    rm /opt/schleuder-cli/Gemfile.lock && \
    bundle install && \
    ln -s /opt/schleuder-cli/bin/schleuder-cli /usr/local/bin/

# entrypoint script
COPY entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

# we need to be able to mount the code into other containers
# so that schleuder work can be run if needed
VOLUME ["/usr/local/bundle", "/etc/schleuder", "/var/lib/schleuder/lists"]

# final config
EXPOSE 4443
USER schleuder
ENTRYPOINT ["/entrypoint.sh"]
CMD ["schleuder-api-daemon"]
